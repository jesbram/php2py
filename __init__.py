import sys
sys.path.append("../")
from tokenizer import Tokenizer
from tokenizer.ajusters.phpSym import PhpSym
Tokenizer.addon(PhpSym)

class Php2py(object):
	"""docstring for Php2py"""
	php_code=[]
	other_code=[]
	startphp=None
	
	def __init__(self,file):
		super(Php2py, self).__init__()
		self.file=file.replace(".php",".py")

		with open(file) as f:
			read=f.read()
			with open(self.file,"w") as f2:
				f2.write(read)
			self.tokenizer=Tokenizer(read)		
		self.vars=[]
	


	def replace_comment(self,block):
		i2=0
		block.replace("'''","\'\'\'").replace('"""','\"\"\"')
		for i in xrange(block.count("/*")):
			i2=block.find("/*")
			f=block.find(" */",i2)
			block=block[:i2]+'"""'+block[i2+3:f].replace('"""',"'")+'"""'+block[f+3:]
		block=block.replace("//","#")
		return block
	def replace_elseif(self,line):
		if line.strip().startswith("else if"):
			return line.replace("else if ","elif ")
		else:
			return line

	def replace_function(self,code):
		
		code=code.replace("static function ","@staticmethod\ndef ")
		code=code.replace("public function ","def ")
		code=code.replace("function ","def ")
		return code
	def replace_quote(self,block):
		return block.replace(";","")
	def replace_boolean(self,block):
		return block.replace("true","True").replace("false","False")
	def relace_convertions(self,block):
		"""
		reemplace (integer) variable por int(variable)
		"""
	def replace_outstring(self,sub,replace,expression):
		"""
		"""
		openstr=""
		_sub=""
		_expression=expression
		for k,elem in enumerate(_expression):
			if openstr and elem==openstr:
				openstr=""
			elif not openstr and (elem=="'" or elem=='"'):
				openstr=elem
			else:
				if elem==sub[0]:
					_sub+=elem
				elif _sub:
					if _sub+elem in sub:
						_sub+=elem
					else:
						_expressionn[k]=replace
						_sub=""
		return _expression




			
	def replace_negation(self,line):
		return line.replace("! ","not ")
	def normalize_comparizon(self,line):
		return line.replace("!==","!=").replace("===","==").replace(" && "," and ").replace(" || "," or ")
	def replace_add_counter(self,line):
		return line.replace(".=","+=").replace("++","+=").replace("--","-=")
	
	def reaplace_realition_arrays(self):
		opens=[]
		open_pos=[]
		for n,line in (tokenizer.tokens):
			tipo=None
			for k,token in line:
				if token=="array" and line[k+1]=="(":
					open_pos.append([n,k,k+1])
					tipo=None
					if k+2<len(line)-1:
						
						for k2,elem in enumerate(line[k+2:]):
							if elem=="=>":
								tipo="dict"
								opens.append(tipo)
								tokenizer.tokens[k][k2]=":"
							elif elem=="," and tipo==None:
								tipo="list"
								opens.append(tipo)
								break
				elif token==")":
					if tipo=="dict":
						tokenizer.tokens[open_pos[-1]]=tokenizer.tokens[open_pos[-1][0]][:open_pos[-1][1]]+["{"]+tokenizer.tokens[open_pos[-1][0]][open_pos[-1][2]+1:]
						tokenizer.tokens[n][k]="}"
					else:
						tokenizer.tokens[open_pos[-1]]=tokenizer.tokens[open_pos[-1][0]][:open_pos[-1][1]]+["["]+tokenizer.tokens[open_pos[-1][0]][open_pos[-1][2]+1:]
						tokenizer.tokens[n][k]="]"

					tipo=opens[-1]

							




						

		with open(self.file) as f:
			read=f.read()
			tokenizer=Tokenizer(read)
			






	def replace_foreach(self):
		pass
	def reaplace_assing_conditional(self,line):
		pattern="([\w|[0-9]]+\s*=\s*[\w|\"|\'|[0-9]])"
		splits=re.split(pattern,line)
		asignaciones=""
		tab=self.gettab(line)
		for k,elem in enumerate(splits):
			if re.findall(pattern,elem):
				variable,asignacion=elem.split("=")
				splits[k]=variable
				asignaciones+=tab+elem+"\n"
		
		return asignaciones+"".join(splits)



		
	def replace_phpembeded(self,phpcode=lambda code:code,othercode=None):
		import re
		with open(self.file) as f:
			read=f.read()
			splits=re.split("(<\?php|[\n|\t|\s+]\?>)",read)
			l=[]
			if "" in splits:
				splits.remove("")
			if splits[0]=="<?php":
				for k,elem in enumerate(splits):
					if k%2!=0:
						if splits[k-1]=="?>":
							tab=self.gettab(elem.split("\n")[-1])
							if callable(othercode):
								self.other_code.append(tab+'print """'+othercode(elem)+'"""')
							else:
								self.other_code.append(tab+'print """'+elem+'"""')
						elif splits[k-1]=="<?php":
							self.php_code.append(phpcode(elem))
	def replace_concatenade(self,block):
		return block.replace(" . ","+")
	def correct_block(self,line):
		if line.strip().startswith("if") and not line.strip().endswith(":"):
			return line+":"
		else:
			return line


	def correct_key_group(self,lines):
		l=[]
		for k,line in enumerate(lines):
			if line.strip()=="{" and lines[k-1].strip().startswith("if"):
				l[-1]+="{"
			elif line.strip()=="{" and lines[k-1].strip().startswith("while"):
				l[-1]+="{"
			else:
				l.append(line)
		return l



	def clean_block_keys(self,lines):
		l=[]
		tabs=[]
		for k,line in enumerate(lines):

			if (line.strip().startswith("if ") or line.strip().startswith("for") or  line.strip().startswith("while") or line.strip().startswith("function")) and line.strip().endswith("{"):
				i=line.rfind("{")
				l.append(line[:i]+":"+line[i+1:])
				tabs.append(self.gettab(line))
			elif line.strip().startswith("} else"):
			 	i=line.rfind("{")
				l.append(line[:i].replace("} ","")+":"+line[i+1:])
				tabs.append(self.gettab(line))
			elif line.strip()=="}" and tabs and tabs[-1]==self.gettab(line):
				del tabs[-1]
			else:
				l.append(line)

		return l
		"""
		open_key=block.find("{")
		close_key=block.find("}",open_key)
		while close_key!=-1:
			expand_key=close_key
			for i in xrange(code[open_key+1:close_key].count("{")):
				expand_key=block.find("}",expand_key)
			close_key=expand_key
			block[:open_key]+":"+block[open_key+1]
		"""
			

			



	def replace_keysgroup(self,lines):
		l=[]
		while_tabs=[]
		counters=[]
		for k,line in enumerate(lines):
			if line.strip().startswith("foreach"):
				if "=>" in line:
					i=line.find("=>")
					i2=line.rfind("as ",i)
					i3=line.find(")",i)
					i4=line.rfind("(",i3)
					iterador=line[i4+1:i2].strip()
					key=line[i2+len("as "):i].strip()
					value=line[i+len("=>"):i3].strip()
					tab=self.gettab(line)
					l.append(tab+"for "+key+" in "+iterador+":")
					tab2=self.gettab(f.readlines()[k+1])
					l.append(tab2+value+"="+iterador+"["+key+"]")
				else:
					i=line.find("=>")
					i2=line.rfind("as ",i)
					i3=line.find(")",i)
					i4=line.rfind("(",i3)
					iterador=line[i4+1:i2].strip()
					key=line[i2+len("as "):i].strip()
					value=line[i+len("=>"):i3].strip()
					tab=self.gettab(line)
					l.append(tab+"for "+key+" in "+iterador+":")
					
			elif line.strip().startswith("for"):
				i=line.find("(")
				f=line.rfind(")")
				tab=self.gettab(line)
				inicial,condition,contador=line[i:f].split(";")
				if contador.endswith("++"):
					contador=contador.replace("++","+=1")
				elif contador.endswith("--"):
					contador=contador.replace("++","-=1")
				counters.append(contador)


				l.append(tab+inicial)
				l.append("while "+condition+":")
				tab2=self.gettab(f.readlines()[k+1])
				while_tabs.append(tab2)
			elif line.strip().startswith("if"):
				pass
			elif line.strip().startswith("elif"):
				pass

			elif line.strip().startswith("}"):
				tab=self.gettab(f.readlines()[k+1])
				if while_tabs[-1]!=tab:
					l.append(tab+counters[-1])
					del counters[-1]
					del  while_tabs[-1]
		return l

			


					



							

	def get_vars_on_str(self,line):
		opener=None
		_vars=[]
		if not self.vars:
			self.get_vars(line)
		for elem in self.vars:
			if "$"+elem in line:
				line=line.replace("$"+elem ,"")
				_vars.append(elem)
		_vars.sort(key=len)
		return _vars
	def filter_strings(self,block,instring=lambda string:string,outstring=lambda nostring:nostring,oncomment=lambda comment:comment,commentSym="#"):
		opener=""
		_vars=[]
		_var_current=[0,[],0]
		code=""
		string=""
		nostring=""
		closer=""
		start=True
		isopen=False
		comment=""
		iscomment=False
		if not self.vars:
			self.get_vars(block)
		

		for i,x in enumerate(block):
			
			if x=="'" or x=='"': 

				if opener:
					string+=x
					
					if len(opener)==3 and block[i-2:i+1]==opener:

						if isopen:
							opener=""
							if nostring:
								code+=outstring(nostring)
								nostring=""

							code+=instring(string)
							string=""
							isopen=False
						else:

							isopen=True

					elif len(opener)==1 and block[i:i+1]==opener:

						opener=""

						if nostring:

							code+=outstring(nostring)
							nostring=""

						code+=instring(string)
						string=""

					
					
				else:
					if not iscomment:
						string+=x
						if block[i+1]==x and block[i+2]==x:

							opener=x*3
							
						else:
							opener=x
						
					elif iscomment:
						comment+=x
					
			else:
				if x=="\n":
					if iscomment:
						comment+=x
		
						nostring+=oncomment(comment)
						print comment
						comment=""
						iscomment=False
					elif opener:
						string+=x

					else:
						nostring+=x
				else:
					if iscomment:
						comment+=x
	

					elif not iscomment and nostring[-len(commentSym):]==commentSym:
						nostring=nostring[:-len(commentSym)]
						iscomment=True
			
						comment=commentSym
						
						

					elif not opener and not iscomment:
						nostring+=x
					
					
					elif opener and not iscomment:
						string+=x


		if nostring:
			code+=outstring(nostring)
			nostring=""
		

		return code

	def replace_vars_on_str(self,block):

		_vars=self.get_vars_on_str(block)

		_format=""
		

		if _vars:

			for elem in _vars:
				_format+=elem+","

			_format=".format("+_format+")"
			
			for k,elem in enumerate(_vars):
				block=block.replace("{"+"$"+elem+"}","{"+str(k)+"}")
			for k,elem in enumerate(_vars):
				block=block.replace("$"+elem,"{"+str(k)+"}")
			


		return block+_format
	def get_vars(self,read):
		import re
		for elem in re.findall("(\$\w+)",read):
			if elem[1:] not in self.vars:
				self.vars.append(elem[1:])
		self.vars.sort(key=len)

	def replace_access_obj(self,block):
		return block.replace("->",".").replace("::",".")



	def replace_vars(self,block):
		import re
		l=[]

		self.get_vars(block)
	
		block=self.filter_strings(block,self.replace_vars_on_str,self.replace_concatenade)

		for elem in self.vars:

			block=block.replace("$"+elem,elem)



		return block
		
	def gettab(self,line):
		tab=""
		for x in line:
			if x=="\t" or x==" ":
				tab+=x
			else:
				break
		return tab


	def move_docstring(self,block):
		opener=False
		docstring=[]
		l=[]

		lines=block.split("\n")
		linetab=None
		isdoc=True
		func=None
		for k,line in enumerate(lines):
			if line.strip().startswith("function ") or line.strip().startswith("static function ") or line.strip().startswith("public function "):
				c=k
				isopen=False
				
				for elem in l:
					c-=1
				
					if lines[c].strip().startswith('"""'):

						if not isopen:
							isopen=True
						else:
							break
			
				docstring=l[c:]
				
				del l[c:]
				linetab=self.gettab(line)
				tab=self.gettab(lines[k+1])
				p=line.rfind("{")
				l.append(line[:p]+":"+line[p+1:])
				l.extend([ tab+doc for doc in docstring])
			else:

				if line!="" and linetab==self.gettab(line):
					linetab=None
					l.append("")
				else:
					l.append(line)


		return "\n".join(l)
	def comment_global(self,code):
		return code.replace("global ","#global ")
	def replace_phpmultiline(self,block):
		import re
		multi=re.findall("(=<<<\w+)",block)

		for elem in multi:
			nom=elem.replace("=<<<","")
			i=block.find(elem)
			f=block.find(nom+";",i+len(elem))
			block=block[:i+1]+'"""'+block[i+len(elem):f-1]+'"""'+block[f+len(nom):]



		return block

	def convert(self):
		
		def filter(code):
			code=self.replace_phpmultiline(code)
			code=self.replace_comment(code)
			code=self.move_docstring(code)
			code=self.replace_function(code)
			code=self.comment_global(code)

			code=self.replace_access_obj(code)

			code=self.replace_vars(code)
			code=self.replace_boolean(code)
			lines=code.split("\n")
			lines=self.correct_key_group(lines)
			lines=self.clean_block_keys(lines)
			a=0
			for k,line in enumerate(lines):
			
				if "! " in line:
					a=k
				line=self.replace_negation(line)
				
				line=self.correct_block(line)
				line=self.normalize_comparizon(line)
				line=self.replace_elseif(line)
				line=self.replace_add_counter(line)
				lines[k]=line#se hace de esta manera ya que solo se puede modificar una sola ves el indice en la cadena cuando esta procede de un split
			code="\n".join(lines)


			
			
			"""
			
			
			"""
			return code

		self.replace_phpembeded(filter)
		string=""
		for k,elem in enumerate(self.php_code):
			string+=elem+"\n"
			if k<len(self.other_code):
				string+=self.other_code[k]+"\n"
		with open(self.file,"w") as f:
			f.write(string)
		


if __name__=="__main__":
	program=Php2py("embed.php")
	program.convert()
					

